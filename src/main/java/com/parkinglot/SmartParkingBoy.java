package com.parkinglot;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class SmartParkingBoy {
    private List<ParkingLot> parkingLots = new ArrayList<>();

    public void addParkingLot(ParkingLot parkingLot) {
        this.parkingLots.add(parkingLot);
    }

    public ParkingTicket park(Car car) {
        return parkingLots.stream()
                .filter(ParkingLot::hasPosition)
                .max(Comparator.comparingInt(ParkingLot::getRemainingCapacity))
                .map(parkingLot -> parkingLot.park(car))
                .orElseThrow(NoAvailablePositionException::new);
    }

    public Car fetch(ParkingTicket ticket) {
        return parkingLots.stream()
                .filter(parkingLot -> parkingLot.hasCorrespondingCar(ticket))
                .map(parkingLot -> parkingLot.fetch(ticket))
                .findFirst()
                .orElseThrow(UnrecognizedParkingTicketException::new);
    }
}
