package com.parkinglot;

import java.math.BigDecimal;
import java.util.HashMap;

public class ParkingLot {
    private HashMap<ParkingTicket, Car> parkingRelation = new HashMap<>();
    private int remainingCapacity;
    private int totalCapacity;

    public ParkingLot(int totalCapacity,int remainingCapacity) {
        this.totalCapacity = totalCapacity;
        this.remainingCapacity = remainingCapacity;
    }

    public ParkingTicket park(Car car) {
        if (hasPosition()) {
            ParkingTicket ticket = new ParkingTicket();
            parkingRelation.put(ticket, car);
            remainingCapacity -= 1;
            return ticket;
        }
        else throw new NoAvailablePositionException();

    }

    public Car fetch(ParkingTicket ticket) {
        if (hasCorrespondingCar(ticket)) {
            Car car = parkingRelation.get(ticket);
            remainingCapacity += 1;
            parkingRelation.remove(ticket);
            return car;
        }
        else throw new UnrecognizedParkingTicketException();
    }

    public boolean hasPosition() {
        return remainingCapacity > 0;
    }

    public boolean hasCorrespondingCar(ParkingTicket ticket) {
        return parkingRelation.containsKey(ticket) && parkingRelation.containsValue(parkingRelation.get(ticket));
    }

    public int getRemainingCapacity() {
        return remainingCapacity;
    }


    public double getRemainingCapacityRate() {
        return (double) remainingCapacity / totalCapacity;
    }

}
