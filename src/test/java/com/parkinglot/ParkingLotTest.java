package com.parkinglot;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import static org.junit.jupiter.api.Assertions.*;

public class ParkingLotTest {
    @Test
    void should_return_a_parking_ticket_when_park_given_a_car_and_a_parking_lot() {
        //given
        ParkingLot parkingLot = new ParkingLot(10, 10);
        Car car = new Car();

        //when
        ParkingTicket ticket = parkingLot.park(car);

        //then
        assertNotNull(ticket);

    }

    @Test
    void should_return_car_when_fetch_given_parking_lot_and_parking_ticket() {
        //given
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot(10, 10);

        //when
        ParkingTicket ticket = parkingLot.park(car);
        Car fetchedCar = parkingLot.fetch(ticket);

        //then
        assertEquals(car, fetchedCar);

    }

    @Test
    void should_return_right_car_when_fetch_car_twice_given_parking_lot_and_two_cars_and_two_tickets() {
        //given
        ParkingLot parkingLot = new ParkingLot(10, 10);
        Car carA = new Car();
        Car carB = new Car();

        //when
        ParkingTicket ticketA = parkingLot.park(carA);
        ParkingTicket ticketB = parkingLot.park(carB);
        Car fetchedCarA = parkingLot.fetch(ticketA);
        Car fetchedCarB = parkingLot.fetch(ticketB);

        //then
        assertEquals(carA, fetchedCarA);
        assertEquals(carB, fetchedCarB);

    }

    @Test
    void should_throw_exception_when_fetch_given_parking_lot_and_a_wrong_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot(10, 10);
        ParkingTicket wrongTicket = new ParkingTicket();
        Car car = new Car();

        //when
        var exception = assertThrows(UnrecognizedParkingTicketException.class, () -> parkingLot.fetch(wrongTicket));

        //then
        assertEquals("Unrecognized Parking Ticket.", exception.getMessage());

    }

    @Test
    void should_throw_exception_when_fetch_given_parking_lot_and_a_used_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot(10, 10);
        Car car = new Car();
        ParkingTicket usedTicket = parkingLot.park(car);
        parkingLot.fetch(usedTicket);

        //when
        var exception = assertThrows(UnrecognizedParkingTicketException.class, () -> parkingLot.fetch(usedTicket));

        //then
        assertEquals("Unrecognized Parking Ticket.", exception.getMessage());

    }

    @Test
    void should_throw_exception_when_park_given_parking_lot_without_position() {
        //given
        ParkingLot fullParkingLot = new ParkingLot(10, 0);
        Car car = new Car();

        //when
        var exception = assertThrows(NoAvailablePositionException.class, () -> fullParkingLot.park(car));

        //then
        assertEquals("No available position.", exception.getMessage());

    }
}
