package com.parkinglot;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class SuperSmartParkingBoyTest {
    @Test
    void should_return_29_when_park_given_2_parking_lots_and_a_car() {
        //given
        ParkingLot parkingLotA = new ParkingLot(50, 30);
        ParkingLot parkingLotB = new ParkingLot(100, 45);

        Car car = new Car();
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy();

        superSmartParkingBoy.addParkingLot(parkingLotA);
        superSmartParkingBoy.addParkingLot(parkingLotB);

        //when
        superSmartParkingBoy.park(car);

        //then
        assertEquals(29,parkingLotA.getRemainingCapacity());

    }

    @Test
    void should_return_28_when_park_given_2_parking_lots_and_a_car() {
        //given
        ParkingLot parkingLotA = new ParkingLot(100, 45);
        ParkingLot parkingLotB = new ParkingLot(50, 29);

        Car car = new Car();
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy();

        superSmartParkingBoy.addParkingLot(parkingLotA);
        superSmartParkingBoy.addParkingLot(parkingLotB);

        //when
        superSmartParkingBoy.park(car);

        //then
        assertEquals(28, parkingLotB.getRemainingCapacity());

    }

    @Test
    void should_return_right_car_when_fetch_cars_in_2_parking_lots_given_2_tickets() {
        //given
        ParkingLot parkingLotA = new ParkingLot(50, 30);
        ParkingLot parkingLotB = new ParkingLot(100, 45);

        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy();
        superSmartParkingBoy.addParkingLot(parkingLotA);
        superSmartParkingBoy.addParkingLot(parkingLotB);

        Car carA = new Car();
        Car carB = new Car();

        ParkingTicket ticketA = parkingLotA.park(carA);
        ParkingTicket ticketB = parkingLotB.park(carB);

        //when
        Car fetchedCarA = superSmartParkingBoy.fetch(ticketA);
        Car fetchedCarB = superSmartParkingBoy.fetch(ticketB);

        //then
        assertEquals(carA, fetchedCarA);
        assertEquals(carB, fetchedCarB);
    }

    @Test
    void should_throw_exception_when_fetch_given_2_parking_lots_and_a_wrong_ticket() {
        //given
        ParkingLot parkingLotA = new ParkingLot(50, 30);
        ParkingLot parkingLotB = new ParkingLot(100, 45);

        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy();
        superSmartParkingBoy.addParkingLot(parkingLotA);
        superSmartParkingBoy.addParkingLot(parkingLotB);

        ParkingTicket wrongTicket = new ParkingTicket();
        Car car = new Car();

        //when
        var exception = assertThrows(UnrecognizedParkingTicketException.class, () -> superSmartParkingBoy.fetch(wrongTicket));

        //then
        assertEquals("Unrecognized Parking Ticket.", exception.getMessage());

    }

    @Test
    void should_throw_exception_when_fetch_given_2_parking_lots_and_a_used_ticket() {
        //given
        ParkingLot parkingLotA = new ParkingLot(50, 30);
        ParkingLot parkingLotB = new ParkingLot(100, 45);

        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy();
        superSmartParkingBoy.addParkingLot(parkingLotA);
        superSmartParkingBoy.addParkingLot(parkingLotB);

        ParkingTicket usedTicket = new ParkingTicket();
        Car car = new Car();

        //when
        var exception = assertThrows(UnrecognizedParkingTicketException.class, () -> superSmartParkingBoy.fetch(usedTicket));

        //then
        assertEquals("Unrecognized Parking Ticket.", exception.getMessage());

    }

    @Test
    void should_throw_exception_when_park_given_2_parking_lots_both_without_position() {
        //given
        ParkingLot parkingLotA = new ParkingLot(50, 0);
        ParkingLot parkingLotB = new ParkingLot(100, 0);

        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy();
        superSmartParkingBoy.addParkingLot(parkingLotA);
        superSmartParkingBoy.addParkingLot(parkingLotB);

        Car car = new Car();

        //when
        var exception = assertThrows(NoAvailablePositionException.class, () -> superSmartParkingBoy.park(car));

        //then
        assertEquals("No available position.", exception.getMessage());

    }

}
