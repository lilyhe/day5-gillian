package com.parkinglot;


import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class StandardParkingBoyTest {
    @Test
    void should_return_4_when_park_given_2_parking_lots_with_5_position_and_10_position_and_a_car() {
        //given
        ParkingLot parkingLotA = new ParkingLot(10, 5);
        ParkingLot parkingLotB = new ParkingLot(10, 10);

        Car car = new Car();
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy();

        standardParkingBoy.addParkingLot(parkingLotA);
        standardParkingBoy.addParkingLot(parkingLotB);

        //when
        ParkingTicket ticket = standardParkingBoy.park(car);

        //then
        assertEquals(4,parkingLotA.getRemainingCapacity());

    }

    @Test
    void should_return_9_when_park_given_2_parking_lots_with_0_position_and_10_position_and_a_car() {
        //given
        ParkingLot parkingLotA = new ParkingLot(10, 0);
        ParkingLot parkingLotB = new ParkingLot(10, 10);

        Car car = new Car();
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy();

        standardParkingBoy.addParkingLot(parkingLotA);
        standardParkingBoy.addParkingLot(parkingLotB);

        //when
        ParkingTicket ticket = standardParkingBoy.park(car);

        //then
        assertEquals(9,parkingLotB.getRemainingCapacity());

    }

    @Test
    void should_return_right_car_when_fetch_cars_in_2_parking_lots_given_2_tickets() {
        //given
        ParkingLot parkingLotA = new ParkingLot(5, 5);
        ParkingLot parkingLotB = new ParkingLot(10, 10);

        StandardParkingBoy standardParkingBoy = new StandardParkingBoy();
        standardParkingBoy.addParkingLot(parkingLotA);
        standardParkingBoy.addParkingLot(parkingLotB);

        Car carA = new Car();
        Car carB = new Car();

        ParkingTicket ticketA = parkingLotA.park(carA);
        ParkingTicket ticketB = parkingLotB.park(carB);

        //when
        Car fetchedCarA = standardParkingBoy.fetch(ticketA);
        Car fetchedCarB = standardParkingBoy.fetch(ticketB);

        //then
        assertEquals(carA, fetchedCarA);
        assertEquals(carB, fetchedCarB);
    }

    @Test
    void should_throw_exception_when_fetch_given_2_parking_lots_and_a_wrong_ticket() {
        //given
        ParkingLot parkingLotA = new ParkingLot(5, 5);
        ParkingLot parkingLotB = new ParkingLot(10, 10);

        StandardParkingBoy standardParkingBoy = new StandardParkingBoy();
        standardParkingBoy.addParkingLot(parkingLotA);
        standardParkingBoy.addParkingLot(parkingLotB);

        ParkingTicket wrongTicket = new ParkingTicket();
        Car car = new Car();

        //when
        var exception = assertThrows(UnrecognizedParkingTicketException.class, () -> standardParkingBoy.fetch(wrongTicket));

        //then
        assertEquals("Unrecognized Parking Ticket.", exception.getMessage());

    }

    @Test
    void should_throw_exception_when_fetch_given_2_parking_lots_and_a_used_ticket() {
        //given
        ParkingLot parkingLotA = new ParkingLot(5, 5);
        ParkingLot parkingLotB = new ParkingLot(10, 10);

        StandardParkingBoy standardParkingBoy = new StandardParkingBoy();
        standardParkingBoy.addParkingLot(parkingLotA);
        standardParkingBoy.addParkingLot(parkingLotB);

        Car car = new Car();

        ParkingTicket usedTicket = standardParkingBoy.park(car);
        standardParkingBoy.fetch(usedTicket);

        //when
        var exception = assertThrows(UnrecognizedParkingTicketException.class, () -> standardParkingBoy.fetch(usedTicket));

        //then
        assertEquals("Unrecognized Parking Ticket.", exception.getMessage());

    }

    @Test
    void should_throw_exception_when_park_given_2_parking_lots_both_without_position() {
        //given
        ParkingLot parkingLotA = new ParkingLot(10, 0);
        ParkingLot parkingLotB = new ParkingLot(10, 0);

        StandardParkingBoy standardParkingBoy = new StandardParkingBoy();
        standardParkingBoy.addParkingLot(parkingLotA);
        standardParkingBoy.addParkingLot(parkingLotB);

        Car car = new Car();

        //when
        var exception = assertThrows(NoAvailablePositionException.class, () -> standardParkingBoy.park(car));

        //then
        assertEquals("No available position.", exception.getMessage());

    }
}
