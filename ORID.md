# day5-Gillian

## Objective: 
Today is to combine all the methods learned this week, mainly Tasking, OOP and TDD to implement a project. And today's exercises are not separated and AC is constantly updated, which makes me have a deeper understanding of the real business project development process.

## Reflective: 
It is hard for me to keep up with the class exercises but this comprehensive exercise was very helpful for developing thinking. And I experienced a real whole agile development process.

## Interpretive: 
I think today's exercise is a lot like the project development in real life. Customers are constantly putting forward new ACs to improve the project. In the process of meeting AC, developers need to consider whether the implementation of new ACs will destroy the previous ACs and anticipate the maintenance and update of the code in the future. This is not only a test of the developer's coding ability, but also a test of our ability to think globally. Also writing test cases requires comprehensive thinking to cover all situations.

## Decisional: 
Do more exercises and communicate with classmates more.


